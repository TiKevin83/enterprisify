import { createTRPCRouter } from "~/server/api/trpc";
import { ticketsRouter } from "./routers/tickets";
import { pointsRouter } from "./routers/points";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  tickets: ticketsRouter,
  points: pointsRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
