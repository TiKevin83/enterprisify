import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "~/server/api/trpc";

export const pointsRouter = createTRPCRouter({
  getTicketPoints: protectedProcedure
    .input(z.object({ ticketId: z.number() }))
    .query(({ ctx, input }) => {
      return ctx.db.point.findMany({
        where: { ticketId: input.ticketId },
        include: { user: true },
      });
    }),

  setPoint: protectedProcedure
    .input(z.object({ point: z.number(), ticketId: z.number() }))
    .mutation(async ({ ctx, input }) => {
      const existingPoint = await ctx.db.point.findFirst({
        where: {
          userId: ctx.session.user.id,
          ticketId: input.ticketId,
        },
      });
      if (existingPoint) {
        return await ctx.db.point.update({
          where: { id: existingPoint.id },
          data: { value: input.point },
        });
      } else {
        return await ctx.db.point.create({
          data: {
            value: input.point,
            user: { connect: { id: ctx.session.user.id } },
            ticket: { connect: { id: input.ticketId } },
          },
        });
      }
    }),
});
