import { z } from "zod";
import { createTRPCRouter, protectedProcedure } from "~/server/api/trpc";
import { emailDomain } from "~/utils/emailDomain";

export const ticketsRouter = createTRPCRouter({
  getOrgTickets: protectedProcedure.query(({ ctx }) => {
    return ctx.db.ticket.findMany({
      where: { protectedDomain: emailDomain(ctx.session.user.email ?? "") },
    });
  }),

  create: protectedProcedure
    .input(z.object({ name: z.string() }))
    .mutation(async ({ ctx, input }) => {
      return await ctx.db.ticket.create({
        data: {
          name: input.name,
          protectedDomain: emailDomain(ctx.session.user.email ?? ""),
        },
      });
    }),

  delete: protectedProcedure
    .input(z.object({ id: z.number() }))
    .mutation(async ({ ctx, input }) => {
      await ctx.db.point.deleteMany({
        where: { ticketId: input.id },
      });
      return await ctx.db.ticket.delete({
        where: { id: input.id },
      });
    }),
});
