export const emailDomain = (email?: string) => {
  return email?.split("@").pop() ?? "";
};
