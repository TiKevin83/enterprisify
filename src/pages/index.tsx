import { signIn, signOut, useSession } from "next-auth/react";
import Head from "next/head";

import { api } from "~/utils/api";
import { FaPlus, FaTrash } from "react-icons/fa6";
import { useState } from "react";
import { emailDomain } from "~/utils/emailDomain";

const PointingBoard = (props: {
  activeTicket: { name: string; id: number };
}) => {
  const { activeTicket } = props;
  const pointOptions = [1, 2, 3, 5, 8];
  const points = api.points.getTicketPoints.useQuery({
    ticketId: activeTicket.id,
  });
  const userPointMutation = api.points.setPoint.useMutation();
  const handleUpdateUserPoints = (newPointValue: number) => {
    void userPointMutation
      .mutateAsync({ ticketId: activeTicket.id, point: newPointValue })
      .then(() => {
        void points.refetch();
      });
  };
  return (
    <div>
      Now Pointing: {activeTicket.name}
      <div>
        {pointOptions.map((point) => {
          return (
            <button
              style={{ background: "grey", marginRight: ".5rem" }}
              key={point}
              onClick={() => {
                handleUpdateUserPoints(point);
              }}
            >
              {point}
            </button>
          );
        })}
        {points.data?.map((point) => {
          return (
            <div key={point.userId}>
              {point.user.image && (
                // eslint-disable-next-line @next/next/no-img-element
                <img
                  className="rounded-full"
                  style={{
                    width: "2rem",
                    height: "2rem",
                    display: "inline-block",
                  }}
                  src={point.user.image}
                  alt={`image for user ${point.user.name}`}
                />
              )}{" "}
              {point.user.name} {point.value}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const OrganizationDashboard = () => {
  const { data: sessionData } = useSession();
  const userDomain = emailDomain(sessionData?.user.email ?? "");

  const tickets = api.tickets.getOrgTickets.useQuery();
  const [activeTicket, setActiveTicket] = useState("");
  const activeTicketId = tickets.data?.find(
    (ticket) => ticket.name === activeTicket,
  )?.id;
  const [newTicketName, setNewTicketName] = useState("");

  const deleteTicketMutation = api.tickets.delete.useMutation();

  const handleDeleteTicket = (id: number) => {
    void deleteTicketMutation.mutateAsync({ id }).then(() => {
      if (id === activeTicketId) {
        setActiveTicket("");
      }
      void tickets.refetch();
    });
  };
  const createTicket = api.tickets.create.useMutation();
  const handleCreateNewTicket = () => {
    void createTicket.mutateAsync({ name: newTicketName }).then(() => {
      void tickets.refetch();
    });
  };

  const setActiveTicketHandler = (ticketName: string) => {
    setActiveTicket((currentActiveTicket) => {
      if (currentActiveTicket === ticketName) {
        return "";
      }
      return ticketName;
    });
  };

  return (
    <div>
      {activeTicket && activeTicketId ? (
        <PointingBoard
          activeTicket={{ name: activeTicket, id: activeTicketId }}
        />
      ) : (
        <></>
      )}
      <h2>Tickets for {userDomain.toUpperCase()}</h2>
      {tickets.data?.map((ticket) => (
        <div key={ticket.id.toString()}>
          <button
            onClick={() => {
              setActiveTicketHandler(ticket.name);
            }}
            style={{
              background: activeTicket === ticket.name ? "blue" : "grey",
            }}
          >
            {ticket.name}
          </button>{" "}
          <button
            onClick={() => {
              handleDeleteTicket(ticket.id);
            }}
          >
            <FaTrash />
          </button>
        </div>
      ))}
      <div>
        <label htmlFor="NewTicket"></label>
        <input
          type="text"
          id="NewTicket"
          value={newTicketName}
          onChange={(e) => {
            setNewTicketName(e.target.value);
          }}
          style={{ color: "black" }}
        ></input>
        <button onClick={handleCreateNewTicket}>
          <FaPlus />
        </button>
      </div>
    </div>
  );
};

export default function Home() {
  const { data: sessionData } = useSession();
  const userDomain = emailDomain(sessionData?.user.email ?? "");
  const Dashboard = () => (userDomain ? <OrganizationDashboard /> : <></>);
  return (
    <>
      <Head>
        <title>Enterprisify</title>
        <meta
          name="description"
          content="Point your agile tickets with Enterprisify"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className=" flex min-h-screen flex-col items-center justify-center bg-gradient-to-b from-[#2e026d] to-[#15162c]">
        <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16 ">
          <h1 className="text-5xl font-extrabold tracking-tight text-white sm:text-[5rem]">
            Enterprisify
          </h1>
          <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:gap-8"></div>
          <div className="flex flex-col items-center gap-2">
            <Dashboard />
            <LoginPortal />
          </div>
        </div>
      </main>
    </>
  );
}

function LoginPortal() {
  const { data: sessionData } = useSession();

  return (
    <div className="flex flex-col items-center justify-center gap-4">
      <p className="text-center text-2xl text-white">
        {sessionData && <span>Logged in as {sessionData.user.name}</span>}
      </p>
      <button
        className="rounded-full bg-white/10 px-10 py-3 font-semibold text-white no-underline transition hover:bg-white/20"
        onClick={sessionData ? () => void signOut() : () => void signIn()}
      >
        {sessionData ? "Sign out" : "Sign in"}
      </button>
    </div>
  );
}
